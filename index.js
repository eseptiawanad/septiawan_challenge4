
const comOptions = ["batu","gunting","kertas"];
let isGameAlreadyPlayed = false;

userChoose = (userPick) => {
if(isGameAlreadyPlayed === false){ 
    const whatComputerChoose = comRandomizeOption();
    const areYouWin = checkWhoIsTheWinner (userPick, whatComputerChoose);

    addingBackGroundToOurChoice(userPick);
    addingBackGroundToComChoice(whatComputerChoose);
    generateResult(areYouWin);
    isGameAlreadyPlayed = true;}
   
};




comRandomizeOption = () => {
    let index = generateRandom(3);
   
    return comOptions[index];

};

function generateRandom(maxLimit = 3){
    let rand = Math.random() * maxLimit;  
    rand = Math.floor(rand);
    return rand;
}

generateResult = (value) => {
    if(value === "win"){
        let winDisplay = document.getElementById("win-display");
        winDisplay.innerHTML = "Player Win";
        
    }else if (value === "lose"){
        let winDisplay = document.getElementById("win-display");
        winDisplay.innerHTML = "Com Win";
    }else {
        let winDisplay = document.getElementById("win-display");
        winDisplay.innerHTML = "Draw";
    }
};

refreshButton = () => {
    isGameAlreadyPlayed = false;
}

checkWhoIsTheWinner = (userInput, comInput) => {
    if(userInput === "batu"){
        if(comInput === "batu"){
            return "draw";
        } else if (comInput === "gunting"){
            return "win";
        } else {
            return "lose";
        }
    }
    else if(userInput === "gunting"){
        if(comInput === "batu"){
            return "lose";
        } else if (comInput === "gunting"){
            return "draw";
        } else {
            return "win";
        }
    }
    else if(userInput === "kertas"){
        if(comInput === "batu"){
            return "win";
        } else if (comInput === "gunting"){
            return "lose";
        } else {
            return "draw";
        }
    }
};

addingBackGroundToOurChoice = (ourChoice) => {
    let batuUser = document.getElementById("user-batu");
    let guntingUser = document.getElementById("user-gunting");
    let kertasUser = document.getElementById("user-kertas");

    let whoIsAlreadyPick = document.getElementsByClassName("being-choosen");
    
    console.log("whoIsAlreadyPick", whoIsAlreadyPick.length);
    if (whoIsAlreadyPick.length > 0){
        whoIsAlreadyPick[0].classList.remove("being-choosen");
    }
    if (ourChoice === "batu"){        
        batuUser.classList.add("being-choosen");
    }else if(ourChoice === "gunting"){
        guntingUser.classList.add("being-choosen");
    }else{        
        kertasUser.classList.add("being-choosen");
    }
};

addingBackGroundToComChoice = (ourChoice) => {
    let batuCom = document.getElementById("com-batu");
    let guntingCom = document.getElementById("com-gunting");
    let kertasCom = document.getElementById("com-kertas");

    let whoIsComputerPick = document.getElementsByClassName("being-choosen-com");
    
    if (whoIsComputerPick.length > 0){
        whoIsComputerPick[0].classList.remove("being-choosen-com");
    }
    if (ourChoice === "batu"){        
        batuCom.classList.add("being-choosen-com");
    }else if(ourChoice === "gunting"){
        guntingCom.classList.add("being-choosen-com");
    }else{        
        kertasCom.classList.add("being-choosen-com");
    }
};

